<?php
session_start();

?>
<!DOCTYPE html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Reservation</title>
<meta name="reservation hotel for malaysia" >
<meta name="zulkarnain" content="gambohnetwork.com.my">
<meta name="copyright" content="Hotel Malaysia, inc. Copyright (c) 2014">
<link rel="stylesheet" href="scss/foundation.css">
<link rel="stylesheet" href="scss/style.css">
<link href='http://fonts.googleapis.com/css?family=Slabo+13px' rel='stylesheet' type='text/css'>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="icon/css/fontello.css">
<link rel="stylesheet" href="icon/css/animation.css"><!--[if IE 7]><link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script src="jquery.backstretch.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
<meta class="foundation-data-attribute-namespace"><meta class="foundation-mq-xxlarge"><meta class="foundation-mq-xlarge"><meta class="foundation-mq-large"><meta class="foundation-mq-medium"><meta class="foundation-mq-small"><style></style><meta class="foundation-mq-topbar"></head>
<body class="fontbody" >
<div class="row foo" style="margin:30px auto 30px auto;" >
<div class="large-12 columns">
		<div class="large-3 columns centerdiv">
			<a href="sessiondestroy.php" class="button round blackblur fontslabo" >1</a>
			<p class="fontgrey">Please select Date</p>
		</div>
		<div class="large-3 columns centerdiv">
			<a href="unsetroomchosen.php" class="button round blackblur fontslabo" >2</a>
			<p class="fontgrey">Select Room</p>
		</div>
		<div class="large-3 columns centerdiv">
			<a href="guestform.php" class="button round  blackblur fontslabo" >3</a>
			<p class="fontgrey">Guest Details</p>
		</div>
		<div class="large-3 columns centerdiv">
			<a href="#" class="button round fontslabo" style="background-color:#F1C232;">4</a>
			<p class="fontgrey">Reservation Complete</p>
		</div>
</div>

</div>
</div>

<div class="row">
	<div class="large-4 columns blackblur fontcolor" style="margin-left:-10px; padding:10px;">

		<div class="large-12 columns " >
		<p><b>Your Reservation</b></p><hr class="line">
				<form name="guestdetails" action="unsetroomchosen.php" method="post" >
				<div class="row">
					<div class="large-12 columns">
						<div class="row">

							<div class="large-5 columns" style="max-width:100%;">
								<span class="fontgreysmall">Booking Id
								</span>
							</div>

							<div class="large-5 columns" style="max-width:100%;">
								<span class="">: <?php echo $_SESSION['booking_id'];?>
								</span>

							</div>
						</div>
						<div class="row">

							<div class="large-5 columns" style="max-width:100%;">
								<span class="fontgreysmall">Check In
								</span>
							</div>

							<div class="large-5 columns" style="max-width:100%;">
								<span class="">: <?php echo $_SESSION['checkin_date'];?>
								</span>

							</div>
						</div>
						<div class="row">
							<div class="large-5 columns" style="max-width:100%;">
								<span class="fontgreysmall">Check Out
								</span>
							</div>

							<div class="large-5 columns" style="max-width:100%;">
								<span class="">: <?php echo $_SESSION['checkout_date'];?>
								</span>

							</div>
						</div>
						<div class="row">
							<div class="large-5 columns" style="max-width:100%;">
								<span class="fontgreysmall">Adults
								</span>
							</div>

							<div class="large-5 columns" style="max-width:100%;">
								<span class="">: <?php echo $_SESSION['adults'];?>
								</span>

							</div>
						</div>
						<div class="row">
							<div class="large-5 columns" style="max-width:100%;">
								<span class="fontgreysmall">Childrens
								</span>
							</div>

							<div class="large-5 columns" style="max-width:100%;">
								<span class="">: <?php echo $_SESSION['childrens'];?>
								</span>

							</div>
						</div>
						<div class="row">
							<div class="large-5 columns" style="max-width:100%;">
								<span class="fontgreysmall" >Night Stay(s)
								</span>
							</div>

							<div class="large-5 columns" style="max-width:100%;">
								<span class="">: <?php echo $_SESSION['total_night'];?>
								</span>

							</div>
						</div>

						<div class="row"><hr>
							<div class="large-6 columns" style="max-width:100%;">
								<span class="fontgreysmall" >Room Selected
								</span>
							</div>

							<div class="large-4 columns" style="max-width:100%;">
								<span class="fontgreysmall">Qty
								</span>

							</div>
						</div>
						<div class="row">
							<div class="large-6 columns" style="max-width:100%;">
								<span class="" ><?php

													foreach ($_SESSION['roomname'] as &$value0 ) {
													echo $value0;
													print "<br>";
													} ;

												?>

								</span>
							</div>

							<div class="large-4 columns" style="max-width:100%;">
								<span class="">
								<?php foreach ($_SESSION['roomqty'] as &$value1 ) {
													echo $value1;
													print "<br>";
													} ;

												?>
								</span>

							</div>
						</div>

					</div>
				</div><br>
				<div class="row">
						<div class="large-12 columns" style="max-width:100%;">
					<!--		<p class="fontgrey borderstyle" style="text-align:center;">15% Deposit Due Now<br>
							<span class="fontslabo " style="font-size:32px; text-align:center;">IDR <?php echo $_SESSION['deposit'];?></span> -->
							<br><span class="fontgrey" style="text-align:center;">Total</span><br>
							<span class="fontslabo" style="font-size:32px; text-align:center;">IDR <?php echo $_SESSION['total_amount'];?>K</span></p>

						</div>

						<div class="large-12 columns" style="max-width:100%;">


						</div>
				</div>



				  <div class="row">
					<div class="large-12 columns" >
						<button name="submit" href="#" class="button small fontslabo" style="background-color:#F1C232; width:100%;">Edit Reservation</button>
					</div>
				  </div>
				</form>
		</div>



	</div>
	<div class="large-8 columns blackblur fontcolor" style="padding:10px">

		<div class="large-12 columns" >
		<p><b>Reservation Complete</b></p><hr class="line">
		<p>Details of your reservation have just been sent to you
		in a confirmation email. Please check your spam folder if you didn't received any email. We look forward to see you soon. In the
		meantime, if you have any questions, feel free to contact us.</p>
		<p>
		<i class="icon-phone" style="font-size:24px"></i> <span class="i-name fontgrey">Phone</span><span class="i-code">&emsp; 0534-3038899</span><br>

        <i class="icon-mail-alt"style="font-size:24px"> </i> <span class="i-name fontgrey">Email</span><span class="i-code">&emsp; info@nevadahotel-ketapang.com</span>
		</p><hr>
		<div class="row">
			<div class="large-12 columns " >
			<p><b>Your Reservation</b></p><hr class="line">
					<form name="guestdetails" action="unsetroomchosen.php" method="post" >
					<div class="row">
						<div class="large-12 columns">

							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >First Name
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['firstname'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >Last Name
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['lastname'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >Email
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['email'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >Phone Number
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['phone'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >Address
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['addressline1'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >Zip
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['postcode'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >City
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['city'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >State
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['state'];?>
									</span>

								</div>
							</div>
							<div class="row">
								<div class="large-5 columns" style="max-width:100%;">
									<span class="fontgreysmall" >Country
									</span>
								</div>

								<div class="large-5 columns" style="max-width:100%;">
									<span class="">: <?php echo $_SESSION['country'];?>
									</span>

								</div>
							</div>



						</div>
					</div><br>
					<div class="row">
							<div class="large-12 columns" style="max-width:100%;">
						<!--		<p class="fontgrey borderstyle" style="text-align:center;">15% Deposit Due Now<br>
								<span class="fontslabo " style="font-size:32px; text-align:center;">IDR <?php echo $_SESSION['deposit'];?></span> -->
								<br><span class="fontgrey" style="text-align:center;">Total</span><br>
								<span class="fontslabo" style="font-size:32px; text-align:center;">IDR <?php echo $_SESSION['total_amount'];?>K</span></p>

							</div>

							<div class="large-12 columns" style="max-width:100%;">


							</div>
					</div>
					</form>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns" >
					<!--form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" >
					<input type="hidden" name="cmd" value="_xclick">
					<input type="hidden" name="business" value="mrzulkarnine@gmail.com">
					<input type="hidden" name="lc" value="MY">
					<input type="hidden" name="item_name" value="15% Hotel Deposit Payment">
					<input type="hidden" name="amount" value="<?php $amount = $_SESSION['total_amount']; print $amount; ?>">
					<input type="hidden" name="currency_code" value="IDR">
					<input type="hidden" name="button_subtype" value="services">
					<input type="hidden" name="no_note" value="0">
					<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
					<img type="image" src="img/paypal.jpg" style="background-color:white; width:32%; height:14%; padding:2px; " ></img>
					<br><button class="button small " border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="width:32%;background-color:#F1C232; ">Pay Room Deposit Now</button>
					<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
					</form-->
					<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" name="hosted_button_id" value="3FWZ42DLC5BJ2">
					<input type="hidden" name="lc" value="MY">
					<input type="hidden" name="item_name" value="15% Hotel Deposit Payment for Booking ID #<?echo $_SESSION['booking_id'];?>">
					<input type="hidden" name="amount" value="<?php $amount = $_SESSION['total_amount']; print $amount; ?>">
					<input type="hidden" name="currency_code" value="IDR">
					<input type="hidden" name="button_subtype" value="services">
					<input type="hidden" name="no_note" value="0">
					<input type="hidden" name="custom" value="<? echo $_SESSION['booking_id'];?>">
					<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
					<img type="image" src="img/paypal.jpg" style="background-color:white; width:32%; height:14%; padding:2px; " ></img>
					<br><button class="button small " border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="width:32%;background-color:#F1C232; ">Pay Room Deposit Now</button>
					<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

					</form>

					<div class="row">
						<br><button class="button small " onClick="doPayment()" border="0" name="submit" alt="" style="width:32%;background-color:#F1C232; ">Midtrans</button>
					</div>
			</div>
		</div>

		</div>



	</div>


</div>

<script>

var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'https://momentjs.com/downloads/moment.js';
document.head.appendChild(script);

function doPayment(){
	var midtransJson = {
	  "transaction_details": {
	    "order_id": "",
	    "gross_amount": 0
	  },
	  "item_details": [{
	    "id": "",
	    "price": 0,
	    "quantity": 1,
	    "name": "",
	    "brand": "",
	    "category": "",
	    "merchant_name": ""
	  }],
	  "customer_details": {
	    "first_name": "",
	    "last_name": "",
	    "email": "",
	    "phone": "",
	    "billing_address": {
	      "first_name": "TEST",
	      "last_name": "MIDTRANSER",
	      "email": "test@midtrans.com",
	      "phone": "081 2233 44-55",
	      "address": "Sudirman",
	      "city": "Jakarta",
	      "postal_code": "12190",
	      "country_code": "IDN"
	    },
	    "shipping_address": {
	      "first_name": "TEST",
	      "last_name": "MIDTRANSER",
	      "email": "test@midtrans.com",
	      "phone": "0 8128-75 7-9338",
	      "address": "Sudirman",
	      "city": "Jakarta",
	      "postal_code": "12190",
	      "country_code": "IDN"
	    }
	  },
	  "enabled_payments": ["credit_card", "mandiri_clickpay", "cimb_clicks",
	    "bca_klikbca", "bca_klikpay", "bri_epay", "echannel", "mandiri_ecash",
	    "permata_va", "bca_va", "bni_va", "other_va", "gopay", "indomaret",
	    "danamon_online", "akulaku"],
	  "credit_card": {
	    "secure": true
	  },
	  "callbacks": {
	    "finish": "https://demo.midtrans.com"
	  },
	  "expiry": {
	    "start_time": "2019-07-06 00:17:08 +0700",
	    "unit": "minutes",
	    "duration": 5
	  },
	  "custom_field1": "custom field 1 content",
	  "custom_field2": "custom field 2 content",
	  "custom_field3": "custom field 3 content"
	}


	midtransJson.transaction_details.order_id = "<?php echo $_SESSION['booking_id'];?>";
	midtransJson.transaction_details.gross_amount = "<?php echo $_SESSION['total_amount'];?>";

	midtransJson.item_details[0].name = "Hotel Tipe A";
	midtransJson.item_details[0].price = midtransJson.transaction_details.gross_amount;
	midtransJson.item_details[0].id = "<?php echo $_SESSION['booking_id'];?>";

	midtransJson.customer_details.first_name = "<?php echo $_SESSION['firstname'];?>";
	midtransJson.customer_details.last_name = "<?php echo $_SESSION['lastname'];?>";
	midtransJson.customer_details.billing_address.address = "<?php echo $_SESSION['addressline1'];?>" ;
	midtransJson.customer_details.email = "<?php echo $_SESSION['email'];?>";


	midtransJson.customer_details.billing_address.first_name = "<?php echo $_SESSION['firstname'];?>";
	midtransJson.customer_details.billing_address.last_name = "<?php echo $_SESSION['lastname'];?>";
	midtransJson.customer_details.billing_address.email = "<?php echo $_SESSION['email'];?>";

	midtransJson.expiry.start_time = moment().format("YYYY-MM-DD HH:mm:ss Z");

	console.log("JSON midtrans : ", midtransJson);

	var xhr = new XMLHttpRequest();
	xhr.withCredentials = true;

	xhr.addEventListener("readystatechange", function () {
	  if (this.readyState === this.DONE) {
			var responseMidtrans = JSON.parse(this.responseText);
			console.log(responseMidtrans);
			 window.location = responseMidtrans.redirect_url
	  }
	});

	xhr.open("POST", "midtransTransaction.php");
	xhr.setRequestHeader("content-type", "application/json");

	xhr.send(JSON.stringify(midtransJson));

	// var responseDariMidtrans = {
	//   "token": "8acdd40c-2b6c-497e-8df7-ab3cd35f2977",
	//   "redirect_url": "https://app.sandbox.midtrans.com/snap/v2/vtweb/8acdd40c-2b6c-497e-8df7-ab3cd35f2977"
	// }
	//
	// window.location = responseDariMidtrans.redirect_url
}

</script>
</body>
</html>
